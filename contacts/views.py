from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.core.mail import send_mail
from .models import Contact
from listings.models import Listing

def contact(request):
  if request.method == 'POST':
    listing_id = request.POST['listing_id']
    listing = request.POST['listing']
    name = request.POST['name']
    email = request.POST['email']
    phone = request.POST['phone']
    message = request.POST['message']
    user_id = request.POST['user_id']
    # realtor_email = request.POST['realtor_email']

    # go through listings and see if id match and find author email
    listing_internal = get_object_or_404(Listing, pk = listing_id)
    realtor_email = listing_internal.author.email
    

    # Check if user has made inquiry already
    if request.user.is_authenticated:
      user_id = request.user.id
      has_contacted = Contact.objects.all().filter(listing_id=listing_id, user_id=user_id)
      if has_contacted:
        messages.error(request, 'You have already made an inquiry for this listing')
        return redirect('/listings/'+listing_id)



 
    contact = Contact(listing=listing, listing_id=listing_id, name=name, email=email, phone=phone, message=message, user_id=user_id)

    contact.save()

    #Send email
    #Get Author of listing user email and send email, 
    #and display lead data
    send_mail(
      'Property Listing Inquiry- NEW LEAD ALERT',
      f'There has been an inquiry for {listing}.\n Here are the details:\n\n Name: {name} \n Email:{email} \n Phone Number:{phone} \n Message:{message} \n\n Sign into PropertyViz for more info. \n\n Thank you for choosing PropertyViz.',
      'admin@propertyviz.com',
      [realtor_email,'admin@propertyviz.com'],
      fail_silently=False
    )

    messages.success(request, 'Your request has been submitted, a realtor will get back to you soon')
    return redirect('/listings/'+listing_id)
  