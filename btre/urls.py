
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls import handler404,handler500
from django.conf.urls.static import static
from pages import views as page_views

urlpatterns = [
    path('',include('pages.urls')),
    path('listings/',include('listings.urls')),
    path('accounts/',include('accounts.urls')),
    path('realtors/',include('realtors.urls')),
    path('contacts/',include('contacts.urls')),
    path('admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = page_views.my_custom_page_not_found_view
handler500 = page_views.server_error_page_view

