from django.urls import path

from . import views

urlpatterns = [
  path('', views.index, name='listings'),
  path('featured',views.featured, name='featured'),
  path('buy',views.buy,name='buy'),
  path('rent',views.rent,name='rent'),
  path('<int:listing_id>',views.listing,name='listing'),
  # path('<slug:slug_text>',views.listing_slug,name='listing-slug'),
  path('search',views.search,name='search'),
  path('create',views.listing_create_form,name='listing-create'),
  path('create-featured',views.listing_create_featured_form,name='listing-create-featured'),
  path('<int:listing_id>/update/',views.listing_update_form,name='listing-update'),
  path('<int:listing_id>/delete/',views.listing_delete,name='listing-delete')
]