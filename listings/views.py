from django.shortcuts import get_object_or_404, render,redirect, reverse
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from listings.choices import price_choices, bedroom_choices, state_choices, category_choices
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from .models import Listing
from .models import Realtor
from .forms import ListingForm
from django.conf import settings
from django.http import HttpResponse
import geocoder
import stripe


stripe.api_key = settings.STRIPE_SECRET_KEY

# Display Listing Data
def index(request):
  listings = Listing.objects.order_by('-list_date').filter(is_published=True)

  paginator = Paginator(listings, 6)
  page = request.GET.get('page')
  paged_listings = paginator.get_page(page)



  context = {
    'listings': paged_listings
  }

  return render(request, 'listings/listings.html', context)

def featured(request):
  listings = Listing.objects.order_by('-list_date').filter(is_published=True).filter(is_featured=True)

  paginator = Paginator(listings, 6)
  page = request.GET.get('page')
  paged_listings = paginator.get_page(page)



  context = {
    'listings': paged_listings
  }

  return render(request, 'listings/featured.html', context)

def buy(request):
  listings = Listing.objects.order_by('-list_date').filter(is_published=True).filter(category="Buy")

  paginator = Paginator(listings, 6)
  page = request.GET.get('page')
  paged_listings = paginator.get_page(page)



  context = {
    'listings': paged_listings
  }

  return render(request, 'listings/buy.html', context)

def rent(request):
  listings = Listing.objects.order_by('-list_date').filter(is_published=True).filter(category="Rent")

  paginator = Paginator(listings, 6)
  page = request.GET.get('page')
  paged_listings = paginator.get_page(page)



  context = {
    'listings': paged_listings
  }

  return render(request, 'listings/rent.html', context)

def listing_slug(request, slug_text):
  listing = Listing.objects.filter(slug=slug_text)
  if listing.exists():
    listing = listing.first()
  else:
    return HttpResponse('<h1>Listing Not Found</h1>')
  realtors = Realtor.objects.all()
  coordinates = geocoder.google(listing.address,key='AIzaSyBTJlvcxNXQSPW7X4wttdlimrZwrXiMjkM')
  maplatlng = coordinates.latlng
  listing_agent = 1
  for realtor in realtors:
    if realtor.id == listing.author.id:
      listing_agent = realtor
  #Add to view_count
  listing.view_count+=1
  listing.save()
  context = {
    'listing': listing,
    'coordinates':maplatlng,
    'listing_agent':listing_agent

  }

  return render(request, 'listings/listing.html', context)

def listing(request, listing_id):
  listing = get_object_or_404(Listing, pk = listing_id)
  realtors = Realtor.objects.all()
  coordinates = geocoder.google(listing.address,key='AIzaSyBTJlvcxNXQSPW7X4wttdlimrZwrXiMjkM')
  maplatlng = coordinates.latlng
  listing_agent = 1
  for realtor in realtors:
    if realtor.id == listing.author.id:
      listing_agent = realtor
  #Add to view_count
  listing.view_count+=1
  listing.save()
  context = {
    'listing': listing,
    'coordinates':maplatlng,
    'listing_agent':listing_agent

  }

  return render(request, 'listings/listing.html', context)

def search(request):

  queryset_list = Listing.objects.order_by('-list_date').filter(is_published=True)


  #Keywords
  if 'keywords' in request.GET:
    keywords = request.GET['keywords']
    if keywords:
      queryset_list = queryset_list.filter(description__icontains=keywords)

  #City
  if 'city' in request.GET:
    city = request.GET['city']
    if city:
      queryset_list = queryset_list.filter(city__iexact=city)

  #State
  if 'state' in request.GET:
    state = request.GET['province']
    if state:
      queryset_list = queryset_list.filter(province__iexact=province)

  #Bedrooms
  if 'category' in request.GET:
    category = request.GET['category']
    if category:
      queryset_list = queryset_list.filter(category__lte=category)

  #Bedrooms
  if 'bedrooms' in request.GET:
    bedrooms = request.GET['bedrooms']
    if bedrooms:
      queryset_list = queryset_list.filter(bedrooms__lte=bedrooms)

  #Price
  if 'price' in request.GET:
    price = request.GET['price']
    if price:
      queryset_list = queryset_list.filter(price__lte=price)

  context = {
    'state_choices' : state_choices,
    'bedroom_choices': bedroom_choices,
    'price_choices' : price_choices,
    'category_choices': category_choices,
    'listings' : queryset_list,
    'values': request.GET
  }

  return render(request, 'listings/search.html', context)

@login_required(login_url='login')
def listing_create_form(request):
  form = ListingForm(request.POST or None, request.FILES or None)
  author = request.user
  if request.method == "POST":
    if form.is_valid():
      form.instance.author = author
      form.save()
      return redirect(reverse("listing", kwargs={
        'listing_id':form.instance.id
      }))
  context = {
      'form':form
    }
  return render(request,"listings/listing_create.html", context)

@login_required(login_url='login')
def listing_create_featured_form(request):
  form = ListingForm(request.POST or None, request.FILES or None)
  author = request.user
  if request.method == "POST":
    charge = stripe.Charge.create(
      amount=4900,
      currency='cad',
      description='PropertyViz - Featured Listing(30 days)',
      source = request.POST['stripeToken']
    )
    if form.is_valid():
      form.instance.author = author
      form.save()
      messages.success(request, 'Payment Approved, your Featured Listing has been created')
      return redirect(reverse("listing", kwargs={
        'listing_id':form.instance.id
      }))
  context = {
      'form':form,
      'key': settings.STRIPE_PUBLISHABLE_KEY
    }
  return render(request,"listings/listing_create_featured.html", context)

@login_required(login_url='login')
def listing_update_form(request,listing_id):
  listing = get_object_or_404(Listing,id=listing_id)
  form = ListingForm(request.POST or None,request.FILES or None, instance = listing)
  author = request.user
  if request.method == "POST":
    if form.is_valid():
      form.instance.author = author
      form.save()
      return redirect(reverse("listing",kwargs={
        'listing_id': form.instance.id
      }))
  context = {
    'form': form,
  }
  return render(request,"listings/listing_create.html",context)
# User CRUD ROUTES Class Based Views 
#LOGIN REQUIRED ROUTES
@login_required(login_url='login')
def listing_create(request):
  if request.method == 'POST':
    author = request.user
    category = request.POST['category']
    title = request.POST['title']
    address = request.POST['address']
    city = request.POST['city']
    province = request.POST['province']
    postal_code = request.POST['postal_code']
    description = request.POST['description']
    price = request.POST['price']
    bedrooms= request.POST['bedrooms']
    bathrooms = request.POST['bathrooms']
    garage = request.POST['garage']
    sqft = request.POST['sqft']
    lot_size = request.POST['lot_size']
    photo_main = request.FILES.get('photo_main',None)
    photo_1 = request.FILES.get('photo_one',None)
    photo_2 = request.FILES.get('photo_two',None)
    photo_3 = request.FILES.get('photo_three',None)
    photo_4 = request.FILES.get('photo_four',None)
    photo_5 = request.FILES.get('photo_five',None)
    photo_6 = request.FILES.get('photo_six',None)

    listing = Listing(author=author,category=category,title=title,address=address,city=city,province=province,postal_code=postal_code,description=description, price=price, bedrooms=bedrooms, bathrooms=bathrooms, garage=garage, sqft=sqft, lot_size=lot_size,photo_main=photo_main,photo_1=photo_1,photo_2=photo_2,photo_3=photo_3,photo_4=photo_4,photo_5=photo_5,photo_6=photo_6)

    listing.save()
    messages.success(request, 'Your listing has been created')
    return redirect('/accounts/dashboard')
  else:
    return render(request,'listings/listing_create.html')   

@login_required(login_url='login')
def listing_create_featured(request):
  if request.method == 'POST':
    charge = stripe.Charge.create(
      amount=4900,
      currency='cad',
      description='PropertyViz - Featured Listing(30 days)',
      source = request.POST['stripeToken']
    )
    author = request.user
    category = request.POST['category']
    title = request.POST['title']
    address = request.POST['address']
    city = request.POST['city']
    province = request.POST['province']
    postal_code = request.POST['postal_code']
    description = request.POST['description']
    price = request.POST['price']
    bedrooms= request.POST['bedrooms']
    bathrooms = request.POST['bathrooms']
    garage = request.POST['garage']
    sqft = request.POST['sqft']
    lot_size = request.POST['lot_size']
    photo_main = request.FILES.get('photo_main',None)
    photo_1 = request.FILES.get('photo_one',None)
    photo_2 = request.FILES.get('photo_two',None)
    photo_3 = request.FILES.get('photo_three',None)
    photo_4 = request.FILES.get('photo_four',None)
    photo_5 = request.FILES.get('photo_five',None)
    photo_6 = request.FILES.get('photo_six',None)

    listing = Listing(author=author,category=category,title=title,address=address,city=city,province=province,postal_code=postal_code,description=description, price=price, bedrooms=bedrooms, bathrooms=bathrooms, garage=garage, sqft=sqft, lot_size=lot_size,photo_main=photo_main,photo_1=photo_1,photo_2=photo_2,photo_3=photo_3,photo_4=photo_4,photo_5=photo_5,photo_6=photo_6,is_featured=True)

    listing.save()
    messages.success(request, 'Payment Approved, your Featured Listing has been created')
    return redirect('/accounts/dashboard')
  else:
    context = {
      'key': settings.STRIPE_PUBLISHABLE_KEY
    }
    return render(request,'listings/listing_create_featured.html',context)

@login_required(login_url='login')  
def listing_update(request, listing_id):
  listing = get_object_or_404(Listing, pk=listing_id)
  if listing.author == request.user:
    if request.method == 'POST':
      listing.category = request.POST['category']
      listing.title = request.POST['title']
      listing.address = request.POST['address']
      listing.city = request.POST['city']
      listing.province = request.POST['province']
      listing.postal_code = request.POST['postal_code']
      listing.description = request.POST['description']
      listing.price = request.POST['price']
      listing.bedrooms= request.POST['bedrooms']
      listing.bathrooms = request.POST['bathrooms']
      listing.garage = request.POST['garage']
      listing.sqft = request.POST['sqft']
      listing.lot_size = request.POST['lot_size']
      listing.save()
      messages.success(request, 'Your listing has been updated.')
      return redirect('/accounts/dashboard')
    else:  
      context = {
        'listing': listing
      }
      return render(request,'listings/listing_update.html',context)
  else:
    messages.error(request,'You are not authorized to update this listing')
    return redirect('/accounts/dashboard')

@login_required(login_url='login')
def listing_delete(request, listing_id):
  listing = get_object_or_404(Listing, pk=listing_id)
  if listing.author == request.user:
    if request.method == 'POST':
      listing.delete()
      messages.success(request,'Your listing has been deleted')
      return redirect('/accounts/dashboard')
  messages.error(request,'You are not authorized to delete this listing')
  return redirect('/accounts/dashboard')
