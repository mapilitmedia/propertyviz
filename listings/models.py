from django.db import models
from datetime import datetime
from realtors.models import Realtor
from django.contrib.auth.models import User
from django.urls import reverse
from django.db.models.signals import pre_save
from btre.utils import unique_slug_generator

class Listing(models.Model):
  realtor = models.ForeignKey(Realtor, on_delete=models.DO_NOTHING, default=1)
  author = models.ForeignKey(User,on_delete=models.CASCADE,default=1)
  view_count = models.IntegerField(default=0)
  title = models.CharField(max_length=200)
  slug = models.SlugField(max_length=250, null=True, blank=True)
  address = models.CharField(max_length=200)
  city = models.CharField(max_length=100)
  province = models.CharField(max_length=100)
  postal_code = models.CharField(max_length=50)
  description = models.TextField(blank=True)
  price = models.IntegerField() 
  bedrooms = models.IntegerField()
  bathrooms = models.DecimalField(max_digits=2, decimal_places=1)
  garage = models.IntegerField(default=0)
  sqft = models.IntegerField()
  lot_size = models.DecimalField(max_digits=5, decimal_places=1)
  photo_main = models.ImageField(upload_to='photos/%Y/%m/%d/')
  photo_1 = models.ImageField(upload_to='photos/%Y/%m/%d/',blank=True,null=True)
  photo_2 = models.ImageField(upload_to='photos/%Y/%m/%d/',blank=True,null=True)
  photo_3 = models.ImageField(upload_to='photos/%Y/%m/%d/',blank=True,null=True)
  photo_4 = models.ImageField(upload_to='photos/%Y/%m/%d/',blank=True,null=True)
  photo_5 = models.ImageField(upload_to='photos/%Y/%m/%d/',blank=True,null=True)
  photo_6 = models.ImageField(upload_to='photos/%Y/%m/%d/',blank=True, null=True)
  is_featured = models.BooleanField(default=False)
  category = models.CharField(max_length=9,choices=(("Buy","Buy"),("Rent","Rent")),default="Buy")
  is_published = models.BooleanField(default=True)
  list_date = models.DateTimeField(default=datetime.now, blank=True)

  def __str__(self):
    return self.title
  
  def get_absolute_url(self):
    return reverse('listing', kwargs={
      'pk': self.pk
    })
  
  def get_update_url(self):
    return reverse('listing-create', kwargs={
      'pk': self.pk
    })

  def get_delete_url(self):
    return reverse('listing-delete', kwargs={
      'pk':self.pk
    })

def slug_generator(sender, instance, *args, **kwargs):
  if not instance.slug:
    instance.slug = unique_slug_generator(instance)

pre_save.connect(slug_generator, sender=Listing)
  










