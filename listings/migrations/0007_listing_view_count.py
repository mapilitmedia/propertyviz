# Generated by Django 2.1.2 on 2019-07-04 14:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('listings', '0006_auto_20190418_1412'),
    ]

    operations = [
        migrations.AddField(
            model_name='listing',
            name='view_count',
            field=models.IntegerField(default=0),
        ),
    ]
