from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.core.mail import send_mail
from listings.choices import price_choices, bedroom_choices, state_choices, category_choices
# Create your views here.

from listings.models import Listing
from realtors.models import Realtor

def index(request):
  listings = Listing.objects.order_by('-list_date').filter(is_published=True).filter(is_featured=True)[:6]
  public_listings = Listing.objects.order_by('-list_date').filter(is_published=True)[:6]
  realtors = Realtor.objects.order_by('-hire_date')
  context = {
    'listings' : listings,
    'public_listings': public_listings,
    'realtors': realtors,
    'state_choices': state_choices,
    'bedroom_choices': bedroom_choices,
    'category_choices':category_choices,
    'price_choices' : price_choices
  }

  return render(request, 'pages/index.html', context)

def about(request):
  #Get all realtors
  realtors = Realtor.objects.order_by('-hire_date')

  #Get MVP
  mvp_realtors = Realtor.objects.all().filter(is_mvp=True)

  context = {
    'realtors' : realtors,
    'mvp_realtors': mvp_realtors
  }

  return render(request, 'pages/about.html', context)

def services(request):
  return render(request,'pages/services.html')

def contact(request):
  if request.method == 'POST':
    name = request.POST['name']
    email = request.POST['email']
    message = request.POST['message']

    #Send email
    send_mail(
      'Contact Us from ' + name,
      "Email:" + email + "\nMessage:" + message,
      'admin@propertyviz.com',
      ['admin@propertyviz.com'],
      fail_silently=False
    )

    messages.success(request, 'Your request has been submitted')
    return redirect('/contact-us')
    
  return render(request,'pages/contact.html')

def my_custom_page_not_found_view(request, exception):
    return render(request,'pages/404.html', status=404)

def server_error_page_view(request):
    return render(request,'pages/500.html')