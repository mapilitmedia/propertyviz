from django.shortcuts import render, get_object_or_404
from .models import Realtor
from listings.models import Listing
# Create your views here.

def index(request):
  # Get all realtors
  realtors = Realtor.objects.order_by('-hire_date')

  context = {
    'realtors':realtors
  }
  return render(request,'realtors/realtors.html',context)

def realtor(request, agent_id):
  realtor = get_object_or_404(Realtor, pk = agent_id)
  listings = Listing.objects.all()
  filtered_listings = []
  for listing in listings:
    if listing.author.id == agent_id:
      filtered_listings.append(listing)

  context = {
    'realtor':realtor,
    'filtered_listings': filtered_listings
  }

  return render(request,'realtors/realtor.html',context)