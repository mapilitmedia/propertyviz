from django.urls import path
from . import views

urlpatterns = [
  path('', views.index, name='realtors'),
  path('<int:agent_id>',views.realtor,name='realtor'),
]